using System;
using System.Collections.Generic;
using Xunit;
using zadanie1;
namespace TestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            int[] tab = new int[] { 1, 3, 5, 7, 9 };
            int n = 6;
            int k = 3;
            var result = Program.Zadanie1(tab, n, k);
            Assert.True(result == 20);
        }
    }
}
