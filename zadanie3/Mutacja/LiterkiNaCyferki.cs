﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mutacja
{
    public class LiterkiNaCyferki
    {
        public int[] Literki(string slowoNiepoprawne, string slowoPoprawne)
        {
            char[] Poprawne = slowoPoprawne.ToCharArray();
            string Niepoprawne = slowoNiepoprawne;

            int dlSlowa = Poprawne.Length;
            int[] Permutacja = new int[Poprawne.Length];


            for (int i = 0; i < dlSlowa; i++)
            {
                char tmp = Niepoprawne[i];
                for (int j = 0; j < dlSlowa; j++)
                {
                    if (tmp == Poprawne[j])
                    {
                        Permutacja[i] = Convert.ToChar(j + 1);
                        Poprawne[j] = '.';
                        break;
                    }
                }
            }

            foreach (int item in Permutacja)
            {
                Console.Write(item);
            }
            Console.WriteLine();

            return Permutacja;
        }
    }
}
