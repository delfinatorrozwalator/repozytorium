﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mutacja
{
    public class CyferkiNaLiterki
    {
        public static char[] Cyferki(string slowoJakieJest, string permutacjaWejsciowa)
        {
            string[] permutacja = permutacjaWejsciowa.Split(";");

            char[] slowoWejsciowe = new char[slowoJakieJest.Length];
            slowoWejsciowe = slowoJakieJest.ToCharArray();
            char[] slowoWyjsciowe = new char[slowoJakieJest.Length];
            int[] IntPermutacja = Array.ConvertAll(permutacja, s => int.Parse(s));

            for (int i = 0; i < slowoJakieJest.Length; i++)
            {
                int tmp = IntPermutacja[i];
                slowoWyjsciowe[tmp-1] = slowoWejsciowe[i];
            }


            foreach (char item in slowoWyjsciowe)
            {
                Console.Write(item);
            }

            return slowoWyjsciowe;
        }
    }
}
