﻿using System;

namespace sizar
{
    class Program
    {
        static void Main(string[] args)
        {
            string Tryb = args[0];
            string Wejscie = args[1];
            string Liczba = args[2];
            string Wyjscie = args[3];


            IszyfratorDeszyfrator SzyfratorDeszyfrator = new Cezar();
            Plikacz plikacz = new Plikacz();

            char[] doKonwersji = plikacz.Wczytywacz(Wejscie);

            Cezar cezar = new Cezar();
            int LiczbaInt = Int32.Parse(Liczba);


            string doZapisania;
            if (Tryb == "Zaszyfruj")
            {
                char[] zaszyfr = cezar.Encript(doKonwersji, LiczbaInt);
                doZapisania = new string(zaszyfr);
            }
            else if (Tryb == "Odszyfruj")
            {
                char[] odszyfr = cezar.Decrypt(doKonwersji, LiczbaInt);
                doZapisania = new string(odszyfr);
            }
            else
            {
                doZapisania = "Bledna komenda Zaszyfruj/Odszyfruj";
            }
            plikacz.Zapisywacz(Wyjscie, doZapisania);

            Console.WriteLine(doZapisania);

            Console.WriteLine();

        }
    }
}
