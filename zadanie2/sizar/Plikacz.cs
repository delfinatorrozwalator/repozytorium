﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sizar
{
    public class Plikacz
    {
        public char[] Wczytywacz(string sciezka)
        {

            string wczytany = File.ReadAllText(sciezka);
            char[] SciezkaWczytania = wczytany.ToCharArray();
            return SciezkaWczytania;
        }

        public void Zapisywacz(string sciezka, string zapiszywanie)
        {
            {
                string file = sciezka;
                File.WriteAllText(file, zapiszywanie);
            }
        }
    }
}
