﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sizar
{
     public interface IszyfratorDeszyfrator
    {
        public char[] Decrypt(char[] DoOdszyfrowania, int Cyferka);
        public char[] Encript(char[] DoZaszyfrowania, int Cyferka);
    }
}
