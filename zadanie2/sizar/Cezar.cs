﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sizar
{
    public class Cezar : IszyfratorDeszyfrator
    {
        public char[] Encript(char[] DoZaszyfrowania, int Cyferka)
        {
            char[] szyfrowane = new char[DoZaszyfrowania.Length];
            int iloscLiter = DoZaszyfrowania.Length;
            int PoprawnaCyferka = Cyferka % 26;
            if (iloscLiter !=0)

            {
                for (int i = 0; i < DoZaszyfrowania.Length; i++)
                {
                    int konik = (DoZaszyfrowania[i]) + PoprawnaCyferka;
                    szyfrowane[i] = (char)konik;
                }

            }
            else
            {
                Console.WriteLine("Plik jest pusty");
            }

            return szyfrowane;
        }


        public char[] Decrypt(char[] DoOdszyfrowania, int Cyferka)
        {
            char[] szyfrowane = new char[DoOdszyfrowania.Length];
            int iloscLiter = DoOdszyfrowania.Length;
            int PoprawnaCyferka = Cyferka % 26;
            if (iloscLiter != 0)

            {
                for (int i = 0; i < DoOdszyfrowania.Length; i++)
                {
                    int konik = (DoOdszyfrowania[i]) - PoprawnaCyferka;
                    szyfrowane[i] = (char)konik;
                }
            }
            else
            {
                Console.WriteLine("Plik jest pusty");
            }
            return szyfrowane;
        }
    }
}
