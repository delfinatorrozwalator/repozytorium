﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Ptaszek
{
    class Wykonywacz
    {
        public static string Liczby(int liczba)
        {

            Dictionary<int, string> slownikDzejson;
            slownikDzejson = new Dictionary<int, string>();

            Dictionary<int, string> slownik;
            slownik = new Dictionary<int, string>()
            {
                { 0, "zero" },
                { 1, "jeden" },
                { 2, "dwa" },
                { 3, "trzy" },
                { 4, "cztery" },
                { 5, "piec" },
                { 6, "szesc" },
                { 7, "siedem" },
                { 8, "osiem" },
                { 9, "dziewiec" },

            };
            var nazwaLiczby = "";
            string[] tabNazwaLiczby=new string[liczba+1];
            for (int j = 1; j <liczba+1; j++)
            {
                int[] tabLiczba = j.ToString().Select(o => Convert.ToInt32(o) - 48).ToArray();
                string tmp="";
                for (int i = 0; i < tabLiczba.Length; i++)
                {
                    if (slownik.TryGetValue(tabLiczba[i], out nazwaLiczby))
                    {
                        tmp = tmp + " " + nazwaLiczby;

                    }
                }
                slownikDzejson.Add(j, tmp);
            }
            var string1 = JsonSerializer.Serialize(slownikDzejson);
            File.WriteAllText(@".\siuras", string1);
            return string1;
        }


    }
}
